# FlightSearchAPI

Flight Search Web API 2 project for FlightSearchApp Angular 6 application

# How to run the project

+ Confirm DB Connection string in Web.config (Using LocalDB by default)
	+ Search for FlightSearchContext to change the connection string

+ Run the project (The database should be automatically generated, using code-first workflow setup)
	+ In case the DB is not created, run the following command in package manager console
		+ Update-Database -StartUpProjectName FlightSearch -ProjectName FlightSearch.Data