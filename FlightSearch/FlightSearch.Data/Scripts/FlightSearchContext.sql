﻿DECLARE @CurrentMigration [nvarchar](max)

IF object_id('[dbo].[__MigrationHistory]') IS NOT NULL
    SELECT @CurrentMigration =
        (SELECT TOP (1) 
        [Project1].[MigrationId] AS [MigrationId]
        FROM ( SELECT 
        [Extent1].[MigrationId] AS [MigrationId]
        FROM [dbo].[__MigrationHistory] AS [Extent1]
        WHERE [Extent1].[ContextKey] = N'FlightSearch.Migrations.Configuration'
        )  AS [Project1]
        ORDER BY [Project1].[MigrationId] DESC)

IF @CurrentMigration IS NULL
    SET @CurrentMigration = '0'

IF @CurrentMigration < '201810061538427_Initial'
BEGIN
    CREATE TABLE [dbo].[Accounts] (
        [Id] [int] NOT NULL IDENTITY,
        [Name] [nvarchar](max) NOT NULL,
        [Surname] [nvarchar](max) NOT NULL,
        [Email] [nvarchar](max) NOT NULL,
        [Username] [nvarchar](max) NOT NULL,
        [Password] [nvarchar](max) NOT NULL,
        CONSTRAINT [PK_dbo.Accounts] PRIMARY KEY ([Id])
    )
    CREATE TABLE [dbo].[__MigrationHistory] (
        [MigrationId] [nvarchar](150) NOT NULL,
        [ContextKey] [nvarchar](300) NOT NULL,
        [Model] [varbinary](max) NOT NULL,
        [ProductVersion] [nvarchar](32) NOT NULL,
        CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY ([MigrationId], [ContextKey])
    )
    INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
    VALUES (N'201810061538427_Initial', N'FlightSearch.Migrations.Configuration',  0x1F8B0800000000000400CD58DB6EE336107D2FD07F10F8D40259339797369076913A7111749D04ABECBED3D258264A912A49A5F6B7ED433FA9BFD0A1EE17DBB193BD1401026B3473E6C299C3B1FFFDFC8FFF6E9D0AEF09B4E14A06E46C724A3C90918AB94C0292DBE59B5FC8BBB73FFEE0DFC4E9DAFB54EB5D383DB49426202B6BB34B4A4DB482949949CA23AD8C5ADA49A452CA6245CF4F4F7FA567671410822096E7F91F7269790AC5033E4E958C20B3391373158330951CDF8405AA77C75230198B202033C193950D81E96835B966964DD0DAC2DA1AE25D09CE30A210C492784C4A6599C5782F3F1A08AD563209331430F1B8C900F5964C18A8F2B86CD50F4DE9F4DCA5445BC31A2ACA8D55E991806717558DE8D0FC4595264D0DB18A37586DBB715917950CC85514293C02E20D7D5D4E85767ADBEA5CA0703093CAFAC4EBEA9C34DD814DE4FE4EBC692E6CAE2190905BCDC489F7902F048FFE80CDA3FA1364207321BA7162A4F8AE2740D183561968BBF900CB2AFADB9878B46F4787868D59C7A64CEC56DA8B73E2DDA173B610D0B441A708A1551A7E07099A59881F98B5A0A5C380A29023EF035FEE7FED0DFB0E47897873B67E0F32B1AB80E047E2CDF81AE25A5245F051729C3C34B23A872D11EEF71AE65A7E17C73729E3E2DBBBC5A1FE4E093F3063FE563AFEDA9E7DDA0EEE789C1DED318E3D5A45D51DC68A12B7CC3756AD1A7153C5D04FB5C40EC1F69902C9B58DA564D8494322DB626EA26BB99C96645E933EDDC1FAFE9C6519D6B3730B54122F2CAF80E99BF0784E4C4B0C1A992DD4D844DB78420660090CDEA26B8C74C6B5B18E1117CC9DD8344E476ADBCE62479D6B8F83720F59B0AD7E6DE03E8F4FBE7F25366734806B6B3AC33453A4B5226368A26AAF889165712B33C1F416869D2A91A772174BEFB32E39B36B5F4A0E4768F8AF0BD2080FC7A9E8AC8B52890EC768B9A90BD34A0F476AB9A68BD44AC7483E1D1CEDB095E8A8970677E9B037F78DF750A5F1DE8CF9609CFD6AB49EDFF446B356AA100F4BF4C4633767E1C65848CB9E0FFF1253C1C11152AD3067922FC1D872DD20B8649D0F96C4FFCFC2468D89C5615BDB375F99B82BEAB34BD191B768774B924F8EBB98FE2965EB9F5FBBF9BC0EACB7CDBC0E6AB8A1BC0E6DB8751C8376DC2631BEE40EDA11F6AD08E5E806245E288CBF8CB35D2D5EB8408C99C4A7DD6F96FE35189EB410EE7BA684C88D680B5AEBDCCAA5AA2B8EA97523AA5506073207CB62ACD095B67CC9228BAF2330A658053F319117CDB480F856DEE736CBED9531902E44EFEB834FF7FB2FB6A47ECCFE7DE69ECC974801C3E49802DCCBDF722EE226EED99616DA01E19AA562058C0A5761844B360DD29D92070255E5BB860CA4E3944748338160E65E86EC095E121B0EE07B4858B4A92F84DD20CF1F44BFECFE35678966A9A9305A7BF76B09753F97BCFD0F9D32FAA160110000 , N'6.1.3-40302')
END

IF @CurrentMigration < '201810081147396_AddMaxLengthToAccountsTableNameSurnameEmailUsernameFields'
BEGIN
    DECLARE @var0 nvarchar(128)
    SELECT @var0 = name
    FROM sys.default_constraints
    WHERE parent_object_id = object_id(N'dbo.Accounts')
    AND col_name(parent_object_id, parent_column_id) = 'Name';
    IF @var0 IS NOT NULL
        EXECUTE('ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [' + @var0 + ']')
    ALTER TABLE [dbo].[Accounts] ALTER COLUMN [Name] [nvarchar](100) NOT NULL
    DECLARE @var1 nvarchar(128)
    SELECT @var1 = name
    FROM sys.default_constraints
    WHERE parent_object_id = object_id(N'dbo.Accounts')
    AND col_name(parent_object_id, parent_column_id) = 'Surname';
    IF @var1 IS NOT NULL
        EXECUTE('ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [' + @var1 + ']')
    ALTER TABLE [dbo].[Accounts] ALTER COLUMN [Surname] [nvarchar](100) NOT NULL
    DECLARE @var2 nvarchar(128)
    SELECT @var2 = name
    FROM sys.default_constraints
    WHERE parent_object_id = object_id(N'dbo.Accounts')
    AND col_name(parent_object_id, parent_column_id) = 'Email';
    IF @var2 IS NOT NULL
        EXECUTE('ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [' + @var2 + ']')
    ALTER TABLE [dbo].[Accounts] ALTER COLUMN [Email] [nvarchar](320) NOT NULL
    DECLARE @var3 nvarchar(128)
    SELECT @var3 = name
    FROM sys.default_constraints
    WHERE parent_object_id = object_id(N'dbo.Accounts')
    AND col_name(parent_object_id, parent_column_id) = 'Username';
    IF @var3 IS NOT NULL
        EXECUTE('ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [' + @var3 + ']')
    ALTER TABLE [dbo].[Accounts] ALTER COLUMN [Username] [nvarchar](100) NOT NULL
    INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
    VALUES (N'201810081147396_AddMaxLengthToAccountsTableNameSurnameEmailUsernameFields', N'FlightSearch.Migrations.Configuration',  0x1F8B0800000000000400CD58DB6E1B37107D2F907F58F029051C51965F5A6395C095ADC268641B5927EFD4EE4822CA25B724D795BE2D0FF9A4FC42877BBF49962F6D0A03867676E6CC853387237DFFFACDFFB08D85F700DA7025A7E47434261EC850455CAEA724B5AB77BF900FEFDFFCE45F45F1D6FB52EA9D393DB494664A36D626E7949A70033133A398875A19B5B2A350C594458A4EC6E35FE9E92905842088E579FEA7545A1E43F6808F332543486CCAC44245204C21C7374186EADDB0184CC2429892B9E0EB8D0D80E97033BA64968DD0DAC2D61AE25D08CE30A200C48A784C4A6599C578CF3F1B08AC56721D242860E27E9700EAAD983050E4715EAB1F9BD278E252A2B5610915A6C6AAF88980A767458D68D7FC599526550DB18A57586DBB735967959C928B30547804C4EBFA3A9F09EDF486EA9CA17030A3C2FAC4EBE99C542D829DE4FE4EBC592A6CAA612A21B59A8913EF2E5D0A1EFE01BB7BF527C8A94C8568068BE1E2BB960045775A25A0EDEE13AC8A14AE23E2D1B61DED1A56660D9B3CBB6B69CF26C4BB41E76C29A0EA85462502AB34FC0E1234B310DD316B414B870159357BDE3BBEDCFFD21B361FCE13F1166CFB11E4DA6E70D2C6384173BE85A89414117C961CC70F8DAC4E6120C2C35E8354CB1FE2F82A665C1C707B36F957DCE264FFA084EF98317F2B1D1DF08C1F5FEED9A7F5F4F667DA711FE3D8A34554CD892C787160C8B16AC59C9B228676AA397600B64D17C8B0752C39CD8E2A26198AB98AAE26749A337AC9FC740FF5FB0B962458CFC6555048BC20BF0766EF82A713639C63D0D00CF063156DE5091980ADA1F3165D63A473AE8D7594B764EEC46651DC531B3A8B3D752E3D76CADD65C1BAFAA581FBDC3FF9F6BD589D5107AEAEE91CD38C91D6B28CA18AAABE277A96D9D5CC04D3030C3B53228DE53E963E649D7366D33E971C8F50F15F13A4121E8F53D05913A5101D8F51735313A6961E8F54734D13A996F6917CDA39DA6E2BD15E2F75EED26E6F1E1AEFAE4AE5BD1AF3CE38FBC5683DBEEEF5662D57211E96E881476ECE829DB110E73D1FFC2566828323A45261C1245F81B1F9BA4170D39A7436C5FFCFD6468D89C471ABDB7FBE327157D44797A227DEA2CD2D493E38EE62BA7F7BBF6C097A35DCD68E33889A6D392F5C615E2DDCEE865202BF8DD9F6E757DD3AFA17E251FBC4A175221FF32989960AE3CFE3ACD790672E1B7DD6F169F3ABA87F0986AF6B08F7C55442E8C6B9062D75AEE54A9515C7D49A11952A9D035980651156E8425BBE62A1C5D7211893AD8D5F9848B3165B42742D6F539BA4F6C2188897A2F555C3A787FD671B553B66FF36714FE63552C03039A600B7F2B7948BA88A7B3ED0427B205CB3140C8251E1DA8C70EB5D8574A3E4914045F92E2101E9F8E71EE2442098B995017B80E7C486B3F811D62CDC9597C77E90C70FA25D76FF92B3B566B129306A7BF7F30A75BFAFBCFF07B4D651F791110000 , N'6.2.0-61023')
END

