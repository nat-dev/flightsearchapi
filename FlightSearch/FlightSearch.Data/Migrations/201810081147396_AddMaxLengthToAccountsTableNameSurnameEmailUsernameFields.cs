namespace FlightSearch.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddMaxLengthToAccountsTableNameSurnameEmailUsernameFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "Surname", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false, maxLength: 320));
            AlterColumn("dbo.Accounts", "Username", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "Username", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Surname", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Name", c => c.String(nullable: false));
        }
    }
}
