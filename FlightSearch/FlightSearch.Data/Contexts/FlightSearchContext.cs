﻿using System.Data.Entity;
using FlightSearch.Data.Entities;

namespace FlightSearch.Data.Contexts
{
    public class FlightSearchContext : DbContext
    {
        public FlightSearchContext() : base("name=FlightSearchContext") // - Name of connectionString in web.config
        {
        }

        public DbSet<Account> Accounts { get; set; }
    }
}