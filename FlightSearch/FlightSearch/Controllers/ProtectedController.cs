﻿using System.Web;
using System.Web.Http;

namespace FlightSearch.Controllers
{
    [Authorize]
    [RoutePrefix("api/Protected")]
    public class ProtectedController : ApiController
    {
        [HttpGet]
        public string TestAuth()
        {
            var usernameOfRequester = HttpContext.Current.User.Identity.Name;
            return $"Hello {usernameOfRequester}! You are currently logged in!";
        }
    }
}
