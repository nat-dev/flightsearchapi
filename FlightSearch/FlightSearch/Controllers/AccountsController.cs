﻿using System;
using System.Data.Entity;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using FlightSearch.Data.Contexts;
using FlightSearch.Data.Entities;
using FlightSearch.Resources;

namespace FlightSearch.Controllers
{
    [RoutePrefix("api/Accounts")]
    public class AccountsController : ApiController
    {
        private readonly FlightSearchContext _context = new FlightSearchContext();

        [HttpPost]
        [Route("LogIn")]
        public async Task<IHttpActionResult> LogIn(LogInAccountResource model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var account = await PerformLogIn(model.Username, model.Password);

            if (account == null)
                return Content(HttpStatusCode.Unauthorized, "Invalid Username or Password");

            var jwtTokenProvider = new JwtTokenProvider();
            var jwtToken = jwtTokenProvider.GenerateAccessToken(account);

            var userCredentials = new UserCredentialsResource
            {
                Username = account.Username,
                Token = jwtToken
            };

            return Ok(userCredentials);
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterAccountResource model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (await UsernameExists(model.Username))
                return BadRequest($"An account with the username '{model.Username}' already exists!");

            if (await EmailExists(model.Email))
                return BadRequest($"An account with the email '{model.Email}' already exists!");

            var newAccount = new Account
            {
                Name = model.Name,
                Surname = model.Surname,
                Email = model.Email,
                Username = model.Username,
                Password = Hash(model.Password)
            };

            _context.Accounts.Add(newAccount);
            await _context.SaveChangesAsync();

            return Ok();
        }

        #region Helpers

        private static string Hash(string input)
        {
            var inputBytes = Encoding.UTF8.GetBytes(input);
            var algorithm = new SHA256CryptoServiceProvider(); // new MD5CryptoServiceProvider();
            var hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        private async Task<bool> UsernameExists(string username)
        {
            var accountWithUsername = await _context.Accounts.FirstOrDefaultAsync(a => a.Username.ToLower().Equals(username.ToLower()));
            return accountWithUsername != null;
        }

        private async Task<bool> EmailExists(string email)
        {
            var accountWithEmail = await _context.Accounts.FirstOrDefaultAsync(a => a.Email.ToLower().Equals(email.ToLower()));
            return accountWithEmail != null;
        }

        private async Task<Account> PerformLogIn(string username, string password)
        {
            var hashPassword = Hash(password);
            username = username.ToLower();

            var account = await _context.Accounts.FirstOrDefaultAsync(
                a =>
                    a.Username.ToLower().Equals(username) &&
                    a.Password.Equals(hashPassword)
            );

            return account;
        }

        #endregion Helpers
    }

}