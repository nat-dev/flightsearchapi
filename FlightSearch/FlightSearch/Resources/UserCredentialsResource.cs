﻿namespace FlightSearch.Resources
{
    public class UserCredentialsResource
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}