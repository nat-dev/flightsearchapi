﻿using System.ComponentModel.DataAnnotations;

namespace FlightSearch.Resources
{
    public class LogInAccountResource
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}