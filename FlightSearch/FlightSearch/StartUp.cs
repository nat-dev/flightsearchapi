﻿using FlightSearch.Auth;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(FlightSearch.Startup))]

namespace FlightSearch
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AuthConfig.Configure(app);
        }
    }
}