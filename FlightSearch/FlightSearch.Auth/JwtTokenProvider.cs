﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using FlightSearch.Data.Entities;

namespace FlightSearch
{
    public class JwtTokenProvider
    {
        private readonly string _appDomain = ConfigurationManager.AppSettings["JwtAppDomain"];
        private readonly string _jwtSecret = ConfigurationManager.AppSettings["JwtSecret"];

        public string GenerateAccessToken(Account user)
        {
            var jwtSecurityToken = new JwtSecurityToken
            (
                issuer: _appDomain,
                audience: _appDomain,
                claims: CreateClaims(user),
                expires: DateTime.UtcNow.Add(TimeSpan.FromDays(7)),
//                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(1)),
                signingCredentials: _jwtSecret.ToIdentitySigningCredentials()
            );

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }

        private IEnumerable<Claim> CreateClaims(Account user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username)
            };

            /* This app won't have roles. */
            //        if (user.Roles != null)
            //        {
            //            foreach (var role in user.Roles)
            //            {
            //                claims.Add(new Claim(ClaimTypes.Role, role));
            //            }
            //        }

            return claims;
        }
    }
}