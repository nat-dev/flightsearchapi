﻿using System;
using System.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security.Jwt;
using Owin;

namespace FlightSearch.Auth
{
    public static class AuthConfig
    {
        public static void Configure(IAppBuilder app)
        {
            var appDomain = ConfigurationManager.AppSettings["JwtAppDomain"];
            var jwtSecret = ConfigurationManager.AppSettings["JwtSecret"];
            
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = jwtSecret.ToSymmetricSecurityKey(),
                    ValidIssuer = appDomain,
                    ValidAudience = appDomain,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(0)
                }
            });
        }
    }
}
